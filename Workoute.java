package ru.maa.workoute;

import java.util.Arrays;

/**
 * Created by User on 22.03.2018.
 */
public class Workoute {
    public static void main(String[] args) {
        int[] cost = {1, 11, 1, 1, 111};
        int time = 3;

        Arrays.sort(cost);
        System.out.println(Arrays.toString(cost));

        int[] expensive = countOf(cost, time);

        int sum = 0;
        for (int i = 0; i < expensive.length; i++) {
            sum += expensive[i];
        }
        System.out.println("Cтоимость наиболее ценных заказов: " + sum);
    }

    private static int[] countOf(int[] array, int time) {
        int[] expensive = new int[time];
        if (time > array.length) {
            time = array.length;
        }
        for (int i = 0; i < time; i++) {
            expensive[i] = array[array.length - i - 1];
        }
        return expensive;
    }

}
